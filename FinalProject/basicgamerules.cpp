//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include <stdbool.h>
#include <vector>
#include "basicgamerules.h"
#include "position.h"
#include "tile.h"
#include "entity.h"
#include "maze.h"

BasicGameRules::BasicGameRules() { }
BasicGameRules::~BasicGameRules() { }

bool BasicGameRules::allowMove(Game *game, Entity *actor, const Position &source, const Position &dest) const { //this checks if a move is allowed by an entity
    
    //check if distance is 1 and if allowed to move onto tile
    int dist = source.distanceFrom(dest);
    MoveResult check_tile = game->getMaze()->getTile(dest)->checkMoveOnto(actor,source,dest);
    if ((dist != 1) || (check_tile == MoveResult::BLOCK)) {
        return false; //return false if distance is not 1 or tile is not allowed to move onto
    }
    
    //check if there is entity at tile, return true if not
    Entity* check_ent = game->getEntityAt(dest);
    if (check_ent == nullptr){
        return true;
    }
    
    //check if the entity is moveable in the same direction as actor
    if (check_ent->hasProperty('v')) {
        int xpos = 2*dest.getX() - source.getX();
        int ypos = 2*dest.getY() - source.getY();
        Position vpos(xpos,ypos);
        MoveResult check_tile_v = game->getMaze()->getTile(vpos)->checkMoveOnto(actor,dest,vpos); //this checks the case where the moveable entity might be blocked
        if (check_tile_v == MoveResult::BLOCK) {
            return false;
        }
        if (game->getEntityAt(vpos)) { //checks if there's an entity at final position of moveable, in which case it is not allowed
            return false;
        }
        
        return true;
    } else if (check_ent->hasProperty('m') || check_ent->hasProperty('h')){ //allow the hero to move onto minautor and vice versa
        return true;
    } else {
        return false;
    }
    
}


    
void BasicGameRules::enactMove(Game *game, Entity *actor, const Position &dest) const { //enacts a move, given an entity and its destination
    Position source = actor->getPosition();
    if (allowMove(game,actor,source,dest)) {
        Entity* check_ent = game->getEntityAt(dest); //checks what entity is at the destination

        if (check_ent != nullptr && check_ent->hasProperty('v')) { //moves the moveable entity as well if there is a moveable entity at destination
            int xpos = 2*dest.getX() - source.getX();
            int ypos = 2*dest.getY() - source.getY();
            Position pos(xpos,ypos);
            check_ent->setPosition(pos);
        }
        actor->setPosition(dest);
    }
}

GameResult BasicGameRules::checkGameResult(Game *game) const { //checks the gameresults
    std::vector<Entity*> heroes = game->getEntitiesWithProperty('h'); //checks where all the heroes are
    std::vector<Entity*> tvec = game->getEntities(); //checks where all entities are
    int counter=0;

    for (std::vector<Entity *>::iterator it = tvec.begin(); it != tvec.end();it++) { //iterates over every character
        if ((*it)->hasProperty('h')) {
            Position hero_pos = (*it)->getPosition();
            for (int n = 0; n < counter; n++) { //this loop checks if any entities that come before it are already on the tile (in case both minotaur and hero are on the same tile, the one that was there before wins)
                if(tvec[n]->hasProperty('m') && (tvec[n]->getPosition()==hero_pos)) {
                    return GameResult::HERO_LOSES;
                }
            }
            if (game->getMaze()->getTile(hero_pos)->getGlyph() == "<") {return GameResult::HERO_WINS;}
            
            std::vector<Entity*> evil = game->getEntitiesWithProperty('m'); //gets all the minotaurs
            for (std::vector<Entity *>::iterator it2 = evil.begin(); it2 != evil.end(); it2++) { //iterates over every minotaur and if any position of a minotaur and hero match, gameresult becomes lose
                if ((*it2)->getPosition() == hero_pos) {return GameResult::HERO_LOSES; }
            }
        }
        counter++;
    }
    return GameResult::UNKNOWN;
}

        
