//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include "mysterycontroller.h"

MysteryController::MysteryController() {
}

MysteryController::~MysteryController() {
}

Direction MysteryController::getMoveDirection(Game *, Entity *) {
  return Direction::NONE;
}

bool MysteryController::isUser() const {
  return false;
}
