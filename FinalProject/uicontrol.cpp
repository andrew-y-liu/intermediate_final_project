//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include <stdbool.h>
#include "uicontrol.h"
#include "ui.h"
#include "game.h"

UIControl::UIControl() { }
UIControl::~UIControl() { }

Direction UIControl::getMoveDirection(Game *game, Entity *entity) { //gets a move direction from user by calling textui
    (void)entity;
  UI *ui = game->getUI()->getInstance();
  Direction dir = ui->getMoveDirection();
  return dir;
}

bool UIControl::isUser() const{
    return true;
}
