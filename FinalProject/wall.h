//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#ifndef WALL_H
#define WALL_H

#include "tile.h"

// A Wall is a tile that no Entity can move onto
class Wall : public Tile {
private:
  // copy constructor and assignment operator are disallowed
  Wall(const Wall &);
  Wall &operator=(const Wall &);

public:
  Wall();
  virtual ~Wall();

  virtual MoveResult checkMoveOnto(Entity *entity, const Position &fromPos, const Position &tilePos) const; //checks if allowed to move onto this tile
  virtual bool isGoal() const;  //returns true if this tile is the goal
  virtual std::string getGlyph() const; //returns character that represents this tile
};

#endif // WALL_H
