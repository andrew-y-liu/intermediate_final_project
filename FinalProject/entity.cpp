//Andrew Liu
//Lambert Kober
//Rongrong Liu

#include "entity.h"
#include <iostream>
#include <sstream>
#include <vector>
#include <stdbool.h>
#include <algorithm>
#include "entitycontroller.h"

Entity::Entity() { }
Entity::~Entity() {
  if (ectr != nullptr) {delete ectr;}
}

void Entity::setGlyph(const std::string &glyph) {
    char c;
    std::istringstream tempstr(glyph); //converting from string to sstream to char
    tempstr >> c;
    cglyph = c;
}

void Entity::setProperties(const std::string &props) {
    char c;
    std::istringstream tempstr(props); //converting from string
    while (tempstr >> c) { //sstream to char
        properties.push_back(c); //store properties in a vector
    }
}


std::string Entity::getGlyph() const {
    std::string outstr(1,cglyph);
    return outstr;
}

std::string Entity::getProperties() const {
    std::string outstr(properties.begin(), properties.end()); //converts vector into a string and returns it
    return outstr;
}

bool Entity::hasProperty(char prop) const {
    return (std::find(properties.begin(),properties.end(),prop) != properties.end()); //uses find to return true or false
}


void Entity::setController(EntityController *controller) {
    ectr = controller;
}

EntityController* Entity::getController() {
    return ectr;
}

void Entity::setPosition(const Position &pos) {
    epos = pos; 
}

Position Entity::getPosition() const {
    return epos;
}



