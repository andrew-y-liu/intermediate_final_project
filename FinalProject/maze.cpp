//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include <fstream>
#include <sstream>
#include <map>
#include "maze.h"
#include "tile.h"
#include "tilefactory.h"
#include "position.h"


Maze::Maze(int w, int h) : width(w), height(h) { }
Maze::~Maze() {
    std::map<Position, Tile*>::iterator it;
    for (it = pos_entity_data.begin();it != pos_entity_data.end();it++) { //iterating over map to delete all entities
        delete it->second;
    }
}

int Maze::getWidth() const{
    return width;
}
int Maze::getHeight() const{
    return height;
}


void Maze::setTile(const Position &pos, Tile *tile) {
    if (pos.getX() < width && pos.getY() < height) { //check if x and y is not out of bound
        pos_entity_data.insert(std::pair<Position, Tile*>(pos,tile)); //insert a tile into the map
    }
}

const Tile *Maze::getTile(const Position &pos) const {
    std::map<Position, Tile*>::const_iterator it = pos_entity_data.find(pos); //using find function
    if (it != pos_entity_data.end()) { //if find function is endptr then it hasn't found anything
        return it->second;
    } else {
        return nullptr;
    }
}



Maze* Maze::read(std::istream &in) {
    std::string line;
    char c;
    int width;
    int height;
    int curr_y=0;

    in >> width >> height; //assume that width and height are properly formatted integers
    
    if ((width == 0) || (height == 0)) {
        return nullptr;
    }
    
    Maze* maze = new Maze(width,height); //creates new Maze to return

    std::getline(in, line); //skip first line of width and height
        
    while (curr_y < height && std::getline(in,line)) { //gets each line until maze ends
      if ((int)line.length() != width) {
            delete maze;
            return nullptr;
        } else {
            int curr_x=0;
            std::istringstream readline(line);
            while (readline >> c) {
                if (c != '.' && c != '<' && c != '#') {
                    delete maze;
                    return nullptr;
                }
                TileFactory *instance = TileFactory::getInstance(); //creates a tile for each character
                if (!instance) {
                    delete maze;
                    return nullptr;
                } else {
                    Position pos(curr_x,curr_y); //position of this tile
                    Tile *t_obj = instance->createFromChar(c);
                    maze->setTile(pos,t_obj);
                    curr_x++;
                }
            }
            curr_y++;
        }
    }
    
    if (curr_y != height) { //checks if the maze dimensions match the dimensions in text file
        delete maze;
        return nullptr;
    }
    
    return maze;
}


// return true if the  position is in bounds (width and height)
bool Maze::inBounds(const Position &pos) const {
    if ((pos.getX() < width) && (pos.getX() > 0) && (pos.getY() < height) && (pos.getY() >0)) {
        return true;
    } else {
        return false;
    }
}
