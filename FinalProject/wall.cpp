//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include <stdbool.h>
#include "wall.h"


Wall::Wall() { }
Wall::~Wall() { }

MoveResult Wall::checkMoveOnto(Entity *, const Position &, const Position &) const {
  return MoveResult::BLOCK; //cant move onto block
}

bool Wall::isGoal() const { //returns true if tile is the goal
    return false;
}

std::string Wall::getGlyph() const { //returns character that represents this tile
  return "#";
}
