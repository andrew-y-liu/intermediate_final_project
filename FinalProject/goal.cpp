//Andrew Liu
//Lambert Kober
//Rongrong Liu

#include <stdio.h>
#include <stdbool.h>
#include "goal.h"


Goal::Goal() { }
Goal::~Goal() { }

bool Goal::isGoal() const {
    return true; //is goal
}

MoveResult Goal::checkMoveOnto(Entity *, const Position &, const Position &) const { //checks if allowed to move onto this tile
  return MoveResult::ALLOW; //all entities allowed to move on goal
}

std::string Goal::getGlyph() const {
  return "<";
}
