//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include <stdbool.h>
#include "floor.h"


Floor::Floor() { }
Floor::~Floor() { }

bool Floor::isGoal() const { //checks if this tile is the goal
    return false;
}

MoveResult Floor::checkMoveOnto(Entity *, const Position &, const Position &) const {
  return MoveResult::ALLOW; //all entities can move on floor
}

std::string Floor::getGlyph() const {
  return ".";
}
