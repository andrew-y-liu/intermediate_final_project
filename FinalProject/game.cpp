//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include "game.h"
#include "entitycontroller.h"
#include "ecfactory.h"
#include <sstream>
#include <vector>
#include "entity.h"
#include "maze.h"
#include "gamerules.h"
#include "position.h"
#include "ui.h"

Game::Game() { }
Game::~Game() {
    for (std::vector<Entity*>::iterator it = tvec.begin(); it != tvec.end(); it++) {
      delete *it;
    }
    if (tmaze != nullptr) {delete tmaze;} //these lines delete maze, ui, and the rules which have been declared in loadgame
    if (tui != nullptr) {delete tui;}
    if (tgrules != nullptr) {delete tgrules;}
}

void Game::setMaze(Maze *maze) { //returns maze pointer
    tmaze = maze;
}

void Game::setUI(UI *ui) { //returns ui pointer
    tui = ui;
}

void Game::setGameRules(GameRules *gameRules) { //returns gamerules point
    tgrules = gameRules;
}


void Game::addEntity(Entity *entity) {
    Position pos = entity->getPosition();
    tvec.push_back(entity); //stores entities in a vector called tvec
}

Entity* Game::getEntityAt(const Position &pos) {
    Entity *z = nullptr;
    std::vector<Entity *> svec = tvec;
    for (std::vector<Entity *>::iterator it = svec.begin(); it !=svec.end(); it++) {
        if ((*it)->getPosition()==pos) {  //iterates through tvec and returns entity that has the input position
            if ((*it)->hasProperty('m')) {return *it;}
            z = *it;
        }
    }
    return z;
}


const Game::EntityVec& Game::getEntities() const { //returns all entities
    return tvec;
}


std::vector<Entity *> Game::getEntitiesWithProperty(char prop) const { //iterates through vector of entities and returns a vector of entities with entities that have that property property
    std::vector<Entity *> cvec = tvec;
    std::vector<Entity *> ovec;
    for (std::vector<Entity *>::iterator it = cvec.begin() ; it !=cvec.end(); it++) {
        if ((*it)->hasProperty(prop)) {
            ovec.push_back(*it); //stores any entity that matches the property in ovec
        }
    }
    return ovec;
}

Maze* Game::getMaze() {
    return tmaze;
}

UI* Game::getUI() {
    return tui;
}

GameRules* Game::getGameRules() {
    return tgrules;
}







void Game::gameLoop() { //loops until game is over
    std::vector<Entity *> lvec = tvec;
    while (tgrules->checkGameResult(this)==GameResult::UNKNOWN) { //check if game is over
        for (std::vector<Entity *>::iterator it = lvec.begin(); it !=lvec.end(); it++) { //let each entity take a turn
            if ((*it)->getController()->isUser()) { //render the game before a user makes a move
                tui->render(this);
            }
            takeTurn(*it); //call taketurn to allow everyone to take a turn
            if (tgrules->checkGameResult(this)==GameResult::HERO_WINS) { //displays endgame message and changes gamestatus
                tui->displayMessage("Hero wins", true);
                tui->render(this);
                break;
            } else if (tgrules->checkGameResult(this)==GameResult::HERO_LOSES) {
                tui->displayMessage("Hero loses", true);
                tui->render(this);
                break;
            }
        }
    }
}


void Game::takeTurn(Entity *actor) { //allows an entity to take a turn
    Direction direction = actor->getController()->getMoveDirection(this,actor); //calls an entitys getMoveDirection, which proposes a direction
    Position finalpos = actor->getPosition().displace(direction); //create finalpos, direction where the entity moves
    bool check = tgrules->allowMove(this, actor, actor->getPosition(), finalpos); //checks if move is allowed
    if (check) { //if moved is allowed, enact it
        tgrules->enactMove(this, actor, finalpos);
    } else {
        if (tui != nullptr && actor->getController()->isUser()) { //had to include this to pass the tests which dont create a new textui it seems
            tui->displayMessage("Illegal move");
        }
    }
}





//in-progress
Game* Game::loadGame(std::istream &in) { //this loads the game in
    Game* game = new Game;//creates new game
    
    //1) Read in the Maze
    Maze* maze = Maze::read(in);
    game->setMaze(maze);

    //2) Read in Entities
    char glyph;
    char entity;
    std::string inputentity;
    std::string properties;
    int xcord;
    int ycord;
    
    while (in >> inputentity >>xcord >>ycord) { //after maze reads to the last line, this reads in the entities and their respective coordiantes
        if (inputentity.length() > 2) { //this solves the case where there are no properties
            properties = inputentity.substr(2,inputentity.length());
        }
        glyph = inputentity[0];
        entity = inputentity[1];
        EntityControllerFactory *instance = EntityControllerFactory::getInstance();
        EntityController *ec_obj = instance->createFromChar(entity); //create new entity from character
        Entity* e_obj = new Entity;
        e_obj->setController(ec_obj); //gives the entity a controller
        
        std::string strglyph(1,glyph);
        e_obj->setGlyph(strglyph);
        e_obj->setProperties(properties);
        const Position* pos = new Position(xcord,ycord); //create new position for the entity
        e_obj->setPosition(*pos);
        delete pos;
        game->addEntity(e_obj);
    }
    
    return game;
}
