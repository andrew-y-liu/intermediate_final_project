//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include "textui.h"
#include "entity.h"
#include "game.h"
#include "maze.h"
#include "tile.h"

TextUI::TextUI() { }
TextUI::~TextUI() { }

//get direction from the user
Direction TextUI::getMoveDirection() {
    std::string dir;
    std::cout << "Your move (u/d/l/r): ";

    //check for direction
    while (std::cin >> dir) {
        if (dir == "u") {
            return Direction::UP;
        } else if (dir=="d") {
            return Direction::DOWN;
        } else if (dir == "l") {
            return Direction::LEFT;
        } else if (dir == "r") {
            return Direction::RIGHT;
        } else {
	  //case of invalid direction input
          std::cout<<"Unknown direction"<<std::endl;
        }
        std::cout << "Your move (u/d/l/r): ";
    }
    return Direction::NONE; // handle non-std input situation
}

//display message at the end of the game
void TextUI::displayMessage(const std::string &msg, bool endgame) {
    (void)endgame;
    message = msg;
    dsp = 1;
}

//output the UI of the current game
void TextUI::render(Game *game) {
    Maze* maze = game->getMaze(); //gets a copy of maze
    int width = maze->getWidth();
    int currx = 0;
    std::map<Position, Tile*> pos_entity_data = maze->get_data();
    std::map<Position, Tile*>::iterator it;
    for (it = pos_entity_data.begin();it != pos_entity_data.end();it++) { //iterates over every tile in maze
        if (currx == width) { //resets after every line of maze
            currx=0;
            std::cout << std::endl;
        }
        Entity* entity = game->getEntityAt(it->first); //first checks if entity exists at coordinate
        if (entity != nullptr) {
            std::cout << entity->getGlyph(); //outputs the entity
        } else { //if entity doesn't exist, outputs the tile instead
            std::cout << it->second->getGlyph();
        }
        currx++;
    }
    std::cout << std::endl;
    if (dsp) {
        std::cout <<": "<< message << std::endl;
        dsp=0;
    }
}
