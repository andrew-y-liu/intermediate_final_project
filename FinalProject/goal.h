//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#ifndef GOAL_H
#define GOAL_H

#include "tile.h"

// The hero wins the game if they reach a Goal tile.
class Goal : public Tile {
private:
  // copy constructor and assignment operator disallowed
  Goal(const Goal &);
  Goal &operator=(const Goal &);

public:
  Goal();
  virtual ~Goal();

  virtual MoveResult checkMoveOnto(Entity *entity, const Position &fromPos, const Position &tilePos) const;//checks if allowed to move onto this tile
  virtual bool isGoal() const; //checks if this tile is the goal
  virtual std::string getGlyph() const;  //returns the character that represents this tile
};

#endif // GOAL_H
