//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#ifndef ASTARCHASEHERO_H
#define ASTARCHASEHERO_H

#include "entitycontroller.h"
#include <map>

class AStarChaseHero : public EntityController {
private:
  // copy constructor and assignment operator are disallowed
    AStarChaseHero(const AStarChaseHero &);
    AStarChaseHero &operator=(const AStarChaseHero &);


public:
    AStarChaseHero();
    virtual ~AStarChaseHero();
 
    virtual Direction getMoveDirection(Game *game, Entity *entity);
    virtual bool isUser() const;

private:
    bool moveValid(Game *game, Position pos, Direction dir);
    Direction first_step(std::map <Position, Position> came_from, Position curr_pos);
    
};





#endif // ASTARCHASEHERO_H
