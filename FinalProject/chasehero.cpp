//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "chasehero.h"
#include "game.h"
#include "entity.h"
#include "gamerules.h"

ChaseHero::ChaseHero() { }
ChaseHero::~ChaseHero() { }

Direction ChaseHero::getMoveDirection(Game *game, Entity *entity) {
  std::vector<Entity *> heroes = game->getEntitiesWithProperty('h');
  Entity * closest_hero;
  int min_distance = -1;
  //find the closest hero by looping through all heroes in the game
  for (std::vector<Entity *>::iterator it = heroes.begin(); it !=heroes.end(); it++) {
        if (min_distance == -1 || (*it)->getPosition().distanceFrom(entity->getPosition()) < min_distance){ 
          closest_hero = *it;
          min_distance = (*it)->getPosition().distanceFrom(entity->getPosition());
    }
  }
  //get the hero's position and this entity's position
  Position hpos = closest_hero->getPosition();
  Position mypos = entity->getPosition();
  Direction dir;
  //if hero is farther vertically than horizontally, then move is vertical

  if(abs(hpos.getX() - mypos.getX()) < abs(hpos.getY() - mypos.getY())){
    dir = verticalMove(hpos, mypos);
  } else{
    //if hero is farther horizontally or is equally far both horizontally and vertically,
    //then move is horizontal
    dir = horizontalMove(hpos, mypos);
  } 
  //check if the movement is allowed
  if(game->getGameRules()->allowMove(game, entity, entity->getPosition(), 
    entity->getPosition().displace(dir))){
    return dir;
  }
  //if the movement isn't allowed, move along the other axis
  if(dir == Direction::UP|| dir == Direction::DOWN){
    dir =  horizontalMove(hpos, mypos);
  }else{
    dir = verticalMove(hpos, mypos);
  } 


  if(game->getGameRules()->allowMove(game, entity, entity->getPosition(), 
    entity->getPosition().displace(dir))){
    return dir;
  }
  //if neither horizontal movement or vertical movement can reduce the distancr
  //return none
  return Direction::NONE;
}

bool ChaseHero::isUser() const{
    return false;
}
//helper function to determine the direction if movement is horizontal
Direction ChaseHero::horizontalMove(Position hpos, Position mypos){
  if(hpos.getX() > mypos.getX()){
      return Direction::RIGHT;
    }
  return Direction::LEFT;
}

//helper function to determine the direction if movement is vertical
Direction ChaseHero::verticalMove(Position hpos, Position mypos){
  if(hpos.getY() > mypos.getY()){
      return Direction::DOWN;
    }
  return Direction::UP;
}
