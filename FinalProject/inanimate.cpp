//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include <stdbool.h>
#include "inanimate.h"


Inanimate::Inanimate() { }
Inanimate::~Inanimate() { }

Direction Inanimate::getMoveDirection(Game *game, Entity *entity) {
    (void)entity; //void cast so compiler doesn't complain
    (void)game;
  return Direction::NONE; //returns none as inanimate objects don't move
}

bool Inanimate::isUser() const{
    return false; //inanimate can never be the user
}
