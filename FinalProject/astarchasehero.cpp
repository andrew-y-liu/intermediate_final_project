//Andrew Liu (yliu327)
//Rongrong Liu (rliu39)
//Lambert Kober (lkober1)

#include <stdio.h>
#include <stdbool.h>
#include "astarchasehero.h"
#include "game.h"
#include "entity.h"
#include "gamerules.h"
#include "entity.h"
#include <map>
#include <algorithm>

AStarChaseHero::AStarChaseHero() { }
AStarChaseHero::~AStarChaseHero() { }

Direction AStarChaseHero::getMoveDirection(Game *game, Entity *entity) {
  std::vector<Entity *> heroes = game->getEntitiesWithProperty('h');
  Entity * closest_hero;
  int min_distance = -1;
  //find the closest hero by looping through all heroes in the game
  for (std::vector<Entity *>::iterator it = heroes.begin(); it !=heroes.end(); it++) {
        if (min_distance == -1 || 
          (*it)->getPosition().distanceFrom(entity->getPosition()) < min_distance){ 
          closest_hero = *it;
          min_distance = (*it)->getPosition().distanceFrom(entity->getPosition());
    }
  }
  //get the hero's position and this entity's position
  Position hpos = closest_hero->getPosition();
  Position mypos = entity->getPosition();

  std::vector<Position> open_set;
  open_set.push_back(mypos);
  std::map<Position, Position> came_from;
  //gscore for a node: the cheapest path from start to node
  std::map<Position, int> gscore;
  gscore[mypos] = 0;
  //fscore for a node: current best guess of how short a path
  //through this node can be
  std::map<Position, int> fscore;
  fscore[mypos] = mypos.distanceFrom(hpos);

  while(open_set.size() != 0){
    int min_fscore = -1;
    Position curr_pos;
    //let the current position be the position with smallest fscore in openset
    for(std::vector<Position>::iterator it = open_set.begin();it != open_set.end();it++){
      if (min_fscore == -1 || (fscore.count(*it) && (fscore[*it] < min_fscore))){
        min_fscore = fscore[*it];
        curr_pos = *it;
      }
    }

    if (curr_pos == hpos){
      Direction dir = first_step(came_from, curr_pos);
//      delete &came_from;
//      delete &gscore;
//      delete &fscore;
      return dir;
    }
    
    open_set.erase(std::find(open_set.begin(), open_set.end(), curr_pos));
    
    if(moveValid(game, curr_pos, Direction::DOWN)){
      Position neighbour = curr_pos.displace(Direction::DOWN);
      int tentative_g = gscore[curr_pos] + 1;
      // tentative_g is the length of path from start to neighbour
      // that pass through current
      if(!gscore.count(neighbour) || tentative_g < gscore[neighbour]){
        //this path is better, update gscore, fscore of neighbour
        gscore[neighbour] = tentative_g;
        came_from[neighbour] = curr_pos;
        fscore[neighbour] = gscore[neighbour] + neighbour.distanceFrom(hpos);
        if(std::find(open_set.begin(), open_set.end(), neighbour)==open_set.end()){
          //if neighbour is not already in open_set, add it to open_set
          open_set.push_back(neighbour);
        }
      }
    }

    if(moveValid(game, curr_pos, Direction::UP)){
      Position neighbour = curr_pos.displace(Direction::UP);
      int tentative_g = gscore[curr_pos] + 1;
      // tentative_g is the length of path from start to neighbour
      // that pass through current
      if(!gscore.count(neighbour) || tentative_g < gscore[neighbour]){
        //this path is better, update gscore, fscore of neighbour
        gscore[neighbour] = tentative_g;
        came_from[neighbour] = curr_pos;
        fscore[neighbour] = gscore[neighbour] + neighbour.distanceFrom(hpos);
        if(std::find(open_set.begin(), open_set.end(), neighbour)==open_set.end()){
          //if neighbour is not already in open_set, add it to open_set
          open_set.push_back(neighbour);
        }
      }
    }

    if(moveValid(game, curr_pos, Direction::RIGHT)){
      Position neighbour = curr_pos.displace(Direction::RIGHT);
      int tentative_g = gscore[curr_pos] + 1;
      // tentative_g is the length of path from start to neighbour
      // that pass through current
      if(!gscore.count(neighbour) || tentative_g < gscore[neighbour]){
        //this path is better, update gscore, fscore of neighbour
        gscore[neighbour] = tentative_g;
        came_from[neighbour] = curr_pos;
        fscore[neighbour] = gscore[neighbour] + neighbour.distanceFrom(hpos);
        if(std::find(open_set.begin(), open_set.end(), neighbour)==open_set.end()){
          //if neighbour is not already in open_set, add it to open_set
          open_set.push_back(neighbour);
        }
      }
    }

    if(moveValid(game, curr_pos, Direction::LEFT)){
      Position neighbour = curr_pos.displace(Direction::LEFT);
      int tentative_g = gscore[curr_pos] + 1;
      // tentative_g is the length of path from start to neighbour
      // that pass through current
      if(!gscore.count(neighbour) || tentative_g < gscore[neighbour]){
        //this path is better, update gscore, fscore of neighbour
        gscore[neighbour] = tentative_g;
        came_from[neighbour] = curr_pos;
        fscore[neighbour] = gscore[neighbour] + neighbour.distanceFrom(hpos);
        if(std::find(open_set.begin(), open_set.end(), neighbour)==open_set.end()){
          //if neighbour is not already in open_set, add it to open_set
          open_set.push_back(neighbour);
        }
      }
    }
  }
  return Direction::NONE;
}


bool AStarChaseHero::isUser() const{
    return false;
}

bool AStarChaseHero::moveValid(Game *game, Position pos, Direction dir){
  Entity * temp = new Entity;
  temp->setPosition(pos);
  bool isValid = game->getGameRules()->allowMove(game, temp, pos, pos.displace(dir));
  delete temp;
  return isValid;
}

Direction AStarChaseHero::first_step(std::map<Position, Position> came_from, Position curr_pos){

  Position next = curr_pos;
  while(came_from.count(curr_pos)){
    next = curr_pos;
    curr_pos = came_from[curr_pos];
  }
  if(curr_pos.displace(Direction::DOWN) == next){
    return Direction::DOWN;
  }
  if(curr_pos.displace(Direction::UP) == next){
    return Direction::UP;
  }
  if(curr_pos.displace(Direction::RIGHT) == next){
    return Direction::RIGHT;
  }
  if(curr_pos.displace(Direction::LEFT) == next){
    return Direction::LEFT;
  }
  return Direction::NONE;
}


